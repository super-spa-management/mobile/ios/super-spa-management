//
//  ServicesRegister.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Resolver
extension Resolver {
    static func registerSSMServices() {
        register { ColorfulLoger() }
            .implements(Loggable.self)
        
        
        register { RealmManager.default as RealmManagerType }
        
        register { ProductsService() }
            .implements(ProductsServiceType.self)
        
        register { MembershipsService() }
            .implements(MembershipsServiceType.self)
    }
}
