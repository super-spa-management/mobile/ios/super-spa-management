// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum BannerTemplate: StoryboardType {
    internal static let storyboardName = "BannerTemplate"

    internal static let bannerTemplateViewController = SceneType<BannerTemplateViewController>(storyboard: BannerTemplate.self, identifier: "BannerTemplateViewController")
  }
  internal enum CarouselTemplate: StoryboardType {
    internal static let storyboardName = "CarouselTemplate"

    internal static let carouselTemplateViewController = SceneType<CarouselTemplateViewController>(storyboard: CarouselTemplate.self, identifier: "CarouselTemplateViewController")
  }
  internal enum CircleCarouselTemplate: StoryboardType {
    internal static let storyboardName = "CircleCarouselTemplate"

    internal static let circleCarouselTemplateViewController = SceneType<CircleCarouselTemplateViewController>(storyboard: CircleCarouselTemplate.self, identifier: "CircleCarouselTemplateViewController")
  }
  internal enum GridViewTemplate: StoryboardType {
    internal static let storyboardName = "GridViewTemplate"

    internal static let gridViewTemplateViewController = SceneType<GridViewTemplateViewController>(storyboard: GridViewTemplate.self, identifier: "GridViewTemplateViewController")
  }
  internal enum Home: StoryboardType {
    internal static let storyboardName = "Home"

    internal static let homeViewController = SceneType<HomeViewController>(storyboard: Home.self, identifier: "HomeViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"

    internal static let mainViewController = SceneType<MainViewController>(storyboard: Main.self, identifier: "MainViewController")
  }
  internal enum Profile: StoryboardType {
    internal static let storyboardName = "Profile"

    internal static let profileViewController = SceneType<ProfileViewController>(storyboard: Profile.self, identifier: "ProfileViewController")
  }
  internal enum ProfileSettings: StoryboardType {
    internal static let storyboardName = "ProfileSettings"

    internal static let profileSettingsViewController = SceneType<ProfileSettingsViewController>(storyboard: ProfileSettings.self, identifier: "ProfileSettingsViewController")
  }
  internal enum Transactions: StoryboardType {
    internal static let storyboardName = "Transactions"

    internal static let transactionsViewController = SceneType<TransactionsViewController>(storyboard: Transactions.self, identifier: "TransactionsViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
