// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal enum Banner {
    internal enum Login {
      internal static let bnLogin = ImageAsset(name: "bn_login")
    }
  }
  internal enum Icon {
    internal enum Common {
      internal enum LoginBy {
        internal static let icSuperSpaManagement = ImageAsset(name: "ic_SuperSpaManagement")
        internal static let icFacebook = ImageAsset(name: "ic_facebook")
        internal static let icGoogle = ImageAsset(name: "ic_google")
      }
      internal static let icIosBack = ImageAsset(name: "ic_ios_back")
      internal static let icIosChecked = ImageAsset(name: "ic_ios_checked")
      internal static let icIosFaceId = ImageAsset(name: "ic_ios_face_id")
      internal static let icIosFingerprint = ImageAsset(name: "ic_ios_fingerprint")
      internal static let icIosSystemComments = ImageAsset(name: "ic_ios_system_comments")
      internal static let icIosSystemFeedback = ImageAsset(name: "ic_ios_system_feedback")
      internal static let icIosSystemLevel = ImageAsset(name: "ic_ios_system_level")
      internal static let icIosSystemMessages = ImageAsset(name: "ic_ios_system_messages")
      internal static let icIosSystemNotification = ImageAsset(name: "ic_ios_system_notification")
      internal static let icIosSystemSettings = ImageAsset(name: "ic_ios_system_settings")
      internal static let icIosSystemShareOurApp = ImageAsset(name: "ic_ios_system_share_our_app")
      internal static let icIosSystemShareWithFriend = ImageAsset(name: "ic_ios_system_share_with_friend")
      internal static let icIosSystemStar = ImageAsset(name: "ic_ios_system_star")
      internal static let icIosUnCheck = ImageAsset(name: "ic_ios_un_check")
    }
    internal enum Merchants {
      internal static let icIosShops = ImageAsset(name: "ic_ios_shops")
    }
    internal enum Scanner {
      internal static let icIosTabberScanner = ImageAsset(name: "ic_ios_tabber_scanner")
    }
    internal enum Transactions {
      internal static let icTransactionHistories = ImageAsset(name: "ic_transaction_histories")
    }
  }
  internal enum Image {
    internal enum Common {
      internal enum PlaceHolder {
        internal static let imProductPlaceholder = ImageAsset(name: "im_product_placeholder")
      }
    }
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
