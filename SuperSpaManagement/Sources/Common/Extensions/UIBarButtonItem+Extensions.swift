//
//  UIBarButtonItem+Extensions.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/10/21.
//

import UIKit

extension UIBarButtonItem {
    static func circleItem(withImage image: UIImage, target: Any?, action: Selector, circleTag: Int) -> UIBarButtonItem {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let tempButton = UIButton(type: .system)
        tempButton.layer.cornerRadius = 32 / 2.0
        tempButton.layer.masksToBounds = true
        tempButton.backgroundColor = Colors.ink300
        tempButton.setImage(image, for: .normal)
        tempButton.frame = CGRect(x: 0, y: 6, width: 32, height: 32)
        tempButton.isUserInteractionEnabled = false
        tempButton.tag = circleTag
        
        view.addSubview(tempButton)
        
        let customButton = UIButton(type: .system)
        customButton.addTarget(target, action: action, for: .touchUpInside)
        customButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        view.addSubview(customButton)
        return UIBarButtonItem(customView: view)
    }
    
    func hiddenCircleBackground(byTag tag: Int) {
        if let button = (self.customView?.subviews.first(where: { $0.tag == tag && $0 is UIButton })) as? UIButton {
            button.backgroundColor = Colors.transparent
            button.setImage(button.currentImage?.color(Colors.ink500), for: .normal)
        }
    }
    
    func showCircleBackground(byTag tag: Int) {
        if let button = (self.customView?.subviews.first(where: { $0.tag == tag && $0 is UIButton })) as? UIButton {
            button.backgroundColor = Colors.ink300
            button.setImage(button.currentImage?.color(Colors.white500), for: .normal)
        }
    }
}


