//
//  Photo+Ultils.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/28/21.
//

import Foundation
import RxSwift
import RxCocoa
import Photos

extension PHPhotoLibrary {
    static var authorized: Observable<Bool> {
        return Observable.create { observable -> Disposable in
            DispatchQueue.main.async {
                if authorizationStatus() == .authorized {
                    observable.onNext(true)
                    observable.onCompleted()
                } else {
                    observable.onNext(false)
                    requestAuthorization { newStatus in
                        observable.onNext(newStatus == .authorized)
                        observable.onCompleted()
                    }
                }
            }
            return Disposables.create()
        }
    }
}

class PhotoReader {
    static func load(savedAssetId: String, size: CGSize = UIScreen.main.bounds.size) -> Observable<UIImage?> {
        
        if savedAssetId.isEmpty {
            return Observable.just(nil)
        }
        
        let imageManager = PHCachingImageManager()
        guard let asset = PHAsset.fetchAssets(withLocalIdentifiers: [savedAssetId], options: PHFetchOptions()).firstObject else {
            return Observable.just(nil)
        }
        
        return Observable.create { observable -> Disposable in
            imageManager.requestImage(for: asset, targetSize: CGSize(width: size.width, height: size.height), contentMode: .aspectFill, options: nil) { image, _ in
                observable.onNext(image)
            }
            return Disposables.create()
        }
    }
}

class PhotoWriter {
    enum Errors: Error {
        case couldNotSavePhoto
    }
    
    static func save(_ image: UIImage) -> Single<String> {
        return Single.create { single -> Disposable in
            
            var savedAssetId: String?
            PHPhotoLibrary.shared().performChanges {
                let request = PHAssetChangeRequest.creationRequestForAsset(from: image)
                savedAssetId = request.placeholderForCreatedAsset?.localIdentifier
            } completionHandler: { success, error in
                DispatchQueue.main.async {
                    if success, let id = savedAssetId {
                        single(.success(id))
                    } else {
                        single(.failure(error ?? Errors.couldNotSavePhoto))
                    }
                }
            }
            return Disposables.create()
        }
    }
}
