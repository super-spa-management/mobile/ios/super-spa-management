//
//  DSButton+Rx.swift
//  DesignSystem
//
//  Created by Khoi Truong Minh on 2/15/20.
//  Copyright © 2020 vinid. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: DSButton {

    public var isLoading: Binder<Bool> { Binder(base) { control, value in control.isLoading = value } }

    public var title: Binder<String?> { Binder(base) { button, title in button.setTitle(title) } }
    
    public var image: Binder<UIImage?> { Binder(base) { button, image in button.setImage(image) } }
    
    public var tap: ControlEvent<Void> { base.rx.controlEvent(.touchUpInside) }
}
