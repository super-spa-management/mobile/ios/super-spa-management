//
//  DSButton+Style.swift
//  DesignSystem
//
//  Created by Khoi Truong Minh on 2/15/20.
//  Copyright © 2020 vinid. All rights reserved.
//

import UIKit

extension DSButton {

    public struct Style {

        private(set) var normalBackgroundColor: UIColor
        private(set) var highlightedBackgroundColor: UIColor
        private(set) var disabledBackgroundColor: UIColor
        private(set) var normalForegroundColor: UIColor
        private(set) var highlightedForegroundColor: UIColor
        private(set) var disabledForegroundColor: UIColor
        private(set) var titleSize: TitleSize
        private(set) var buttonSize: ButtonSize
        private(set) var imagePosition: ImagePosition
        private(set) var hasLoadingIndicator: Bool
        private(set) var loadingTitle: String?
        private(set) var hasMargin: Bool
        let dimWhenHighlighted: Bool
        let isTitleHidden: Bool
        let cornerRadius: CGFloat
        let textPadding: CGFloat
        let contentPadding: CGFloat

        public init(normalBackgroundColor: UIColor,
                    highlightedBackgroundColor: UIColor,
                    disabledBackgroundColor: UIColor,
                    normalForegroundColor: UIColor,
                    highlightedForegroundColor: UIColor,
                    disabledForegroundColor: UIColor,
                    titleSize: TitleSize,
                    buttonSize: ButtonSize,
                    imagePosition: ImagePosition,
                    hasLoadingIndicator: Bool,
                    loadingTitle: String?,
                    hasMargin: Bool,
                    dimWhenHighlighted: Bool,
                    isTitleHidden: Bool,
                    cornerRadius: CGFloat,
                    textPadding: CGFloat,
                    contentPadding: CGFloat) {
            self.normalBackgroundColor = normalBackgroundColor
            self.highlightedBackgroundColor = highlightedBackgroundColor
            self.disabledBackgroundColor = disabledBackgroundColor
            self.normalForegroundColor = normalForegroundColor
            self.highlightedForegroundColor = highlightedForegroundColor
            self.disabledForegroundColor = disabledForegroundColor
            self.titleSize = titleSize
            self.buttonSize = buttonSize
            self.imagePosition = imagePosition
            self.hasLoadingIndicator = hasLoadingIndicator
            self.loadingTitle = loadingTitle
            self.hasMargin = hasMargin
            self.dimWhenHighlighted = dimWhenHighlighted
            self.isTitleHidden = isTitleHidden
            self.cornerRadius = cornerRadius
            self.textPadding = textPadding
            self.contentPadding = contentPadding
        }

        public func setImagePosition(_ imagePosition: ImagePosition) -> Style {
            var newStyle = self
            newStyle.imagePosition = imagePosition
            return newStyle
        }

        public func setTitleSize(_ titleSize: TitleSize) -> Style {
            var newStyle = self
            newStyle.titleSize = titleSize
            return newStyle
        }

        public func setButtonSize(_ buttonSize: ButtonSize) -> Style {
            var newStyle = self
            newStyle.buttonSize = buttonSize
            return newStyle
        }
        
        public func setForegroundColor(normal: UIColor? = nil, highlight: UIColor? = nil, disabled: UIColor? = nil) -> Style {
            var newStyle = self
            if let color = normal {
                newStyle.normalForegroundColor = color
            }
            if let color = highlight {
                newStyle.highlightedForegroundColor = color
            }
            if let color = disabled {
                newStyle.disabledForegroundColor = color
            }
            return newStyle
        }
        
        public func setBackgroundColor(normal: UIColor? = nil, highlight: UIColor? = nil, disabled: UIColor? = nil) -> Style {
            var newStyle = self
            if let color = normal {
                newStyle.normalBackgroundColor = color
            }
            if let color = highlight {
                newStyle.highlightedBackgroundColor = color
            }
            if let color = disabled {
                newStyle.disabledBackgroundColor = color                
            }
            return newStyle
        }

        /**
         Using to decide on the display loading on DSButton.
         The default is true. Loading view attached to DSButton.
         If it is false. Loading can not appear on DSButton
         */
        public func setHasLoadingIndicator(_ hasLoadingIndicator: Bool) -> Style {
            var newStyle = self
            newStyle.hasLoadingIndicator = hasLoadingIndicator
            return newStyle
        }

        /// Using to set the title with loading state. The default is currentTitle.
        public func setLoadingTitle(_ loadingTitle: String?) -> Style {
            var newStyle = self
            newStyle.loadingTitle = loadingTitle
            return newStyle
        }
    }

    public enum ImagePosition {
        case left
        case right
    }

    public enum ButtonSize {
        case normal
        case small
        case compact
    }

    public enum TitleSize {
        case normal
        case compact
    }
}

extension DSButton.Style {

    /// Primary style
    public static let primary: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.primarya500,
        highlightedBackgroundColor: Colors.primarya500,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.white500,
        highlightedForegroundColor: Colors.white500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// PrimaryNotice style
    public static let primaryNotice: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.green500,
        highlightedBackgroundColor: Colors.green500,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.white500,
        highlightedForegroundColor: Colors.white500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// PrimaryDestruction style
    public static let primaryDestruction: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.red400,
        highlightedBackgroundColor: Colors.red400,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.white500,
        highlightedForegroundColor: Colors.white500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// Secondary style
    public static let secondary: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.primarya200,
        highlightedBackgroundColor: Colors.primarya200,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.primarya500,
        highlightedForegroundColor: Colors.primarya500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// SecondaryDestruction style
    public static let secondaryDestruction: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.red200,
        highlightedBackgroundColor: Colors.red200,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.red500,
        highlightedForegroundColor: Colors.red500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// SecondaryWhite style
    public static let secondaryWhite: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.white200,
        highlightedBackgroundColor: Colors.white200,
        disabledBackgroundColor: Colors.ink200,
        normalForegroundColor: Colors.white500,
        highlightedForegroundColor: Colors.white500,
        disabledForegroundColor: Colors.ink300,
        titleSize: .compact,
        buttonSize: .compact,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 8,
        contentPadding: 0
    )

    /// Plain style
    public static let plain: DSButton.Style = DSButton.Style(
        normalBackgroundColor: .clear,
        highlightedBackgroundColor: .clear,
        disabledBackgroundColor: .clear,
        normalForegroundColor: Colors.primarya500,
        highlightedForegroundColor: Colors.primarya500.withAlphaComponent(0.8),
        disabledForegroundColor: Colors.ink300,
        titleSize: .normal,
        buttonSize: .normal,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: false,
        dimWhenHighlighted: false,
        isTitleHidden: false,
        cornerRadius: 8,
        textPadding: 4,
        contentPadding: 0
    )

    /// Icon style
    public static let icon: DSButton.Style = DSButton.Style(
        normalBackgroundColor: Colors.ink300,
        highlightedBackgroundColor: Colors.ink300,
        disabledBackgroundColor: Colors.ink300,
        normalForegroundColor: Colors.white500,
        highlightedForegroundColor: Colors.white500,
        disabledForegroundColor: Colors.white500,
        titleSize: .normal,
        buttonSize: .compact,
        imagePosition: .left,
        hasLoadingIndicator: true,
        loadingTitle: nil,
        hasMargin: true,
        dimWhenHighlighted: true,
        isTitleHidden: true,
        cornerRadius: 16,
        textPadding: 0,
        contentPadding: 0
    )
}

extension DSButton.ButtonSize {

    var height: CGFloat {
        switch self {
        case .normal: return 48.0
        case .small: return 40.0
        case .compact: return 32.0
        }
    }

    var minWidth: CGFloat {
        switch self {
        case .normal: return 128.0
        case .small: return 128.0
        case .compact: return 80.0
        }
    }
}

extension DSButton.TitleSize {

    var font: UIFont {
        switch self {
        case .normal: return DS.T16M20().font
        case .compact: return DS.T14M().font
        }
    }
}
