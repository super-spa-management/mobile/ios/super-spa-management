//
//  DSButton+Tracking.swift
//  DesignSystem
//
//  Created by TAMPT12 on 3/11/20.
//

import Foundation

extension DSButton {

    /// The data what do you want tracking
    public struct TrackingData {
        /**
         Format: Domain.sub_domain.screen_name.button

         - Domain: The name of domain like: Vincart, Ticket, VinPay, Voucher
         - sub_domain: The project name or module use DSButton like: fund_in; fund_out; topup_point…
         - screen_name: The name of screen use DSButton
         - button: The button name like: add_button, submit_button, continue_button

         Ex:
         ````
         VinCart.multi_merchant.cart_review.continue_button
         ````
         */
        public var id: String

        /**
         The Dictionary

         Ex:
         ````
         [
             "cart_item_count": 12,
             "delivery_method": "home_delivery",
             "shopping_method": "in_store",
             "total_amount": 12345
         ]
         ````
         */
        public var userInfo: [String: Any]

        public init(id: String, userInfo: [String: Any]) {
            self.id = id
            self.userInfo = userInfo
        }
    }

    struct AutoTrackingParam {
        var timestamp: Int64
        var buttonTitle: String
        var component: String

        var autoParams: [String: Any] {
            var params = [String: Any]()
            params["timestamp"] = timestamp
            params["button_title"] = buttonTitle
            params["component"] = component
            return params
        }
    }
}
