//
//  DSButton.swift
//  DesignSystem
//
//  Created by Que Nguyen on 11/02/19.
//  Copyright © 2020 Que Nguyen. All rights reserved.
//

import SnapKit

extension UIImage {
    func compressImage(maxSize: CGSize, compressionQuality: CGFloat) -> Data? {
        var actualHeight = Double(self.size.height)
        var actualWidth = Double(self.size.width)
        let maxHeight = Double(maxSize.height)
        let maxWidth = Double(maxSize.width)
        var imgRatio: Double = actualWidth / actualHeight
        let maxRatio: Double = maxWidth / maxHeight
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                // adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                // adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: compressionQuality)
        UIGraphicsEndImageContext()
        
        return imageData
    }
    
    final var template: UIImage {
        return self.withRenderingMode(.alwaysTemplate)
    }
    
    final var original: UIImage {
        self.withRenderingMode(.alwaysOriginal)
    }
    
    func color(_ color: UIColor) -> UIImage {
        let templateImage = self.template
        let imageRenderer = UIGraphicsImageRenderer(size: templateImage.size, format: template.imageRendererFormat)
        let tintedImage = imageRenderer.image { _ in
            color.set()
            templateImage.draw(at: .zero)
        }
        return tintedImage.original
    }
}

public final class DSButton: UIControl, DS2BaseViewProtocol {

    /// The custom data want to tracking
    public var trackingData: TrackingData?

    /**
     Using to set the style for DSButton.
     The default is primary.
     You can set another style.
     */
    @discardableResult
    public func setStyle(_ style: Style) -> DSButton {
        self.style = style
        configureStyle()
        invalidateIntrinsicContentSize()
        layout()
        return self
    }

    /**
     Use to start/stop loading animation
     true: start  animation
     false: stop loading animation
     */
    public var isLoading: Bool = false {
        didSet {
            updateLayout(isLoading)
        }
    }

    /// The method set loading with out animation
    /// - Parameters:
    ///   - isLoading: The boolean value is loading
    ///   - animated: The boolean value is animate
    public func setLoading(_ isLoading: Bool, animated: Bool) {
        updateLayout(isLoading, animated: animated)
    }

    /// Using to set title for DSButton
    /// - Parameter title: string title
    public func setTitle(_ title: String?) {
        _title = title
    }

    /// Using to set image for DSButton
    /// - Parameter image: The image
    public func setImage(_ image: UIImage?) {
        _image = image?.color(style.normalForegroundColor)
    }

    @available(*, deprecated, message: "Use setTitle(_ title: String?) instead")
    public func setTitle(_ title: String?, for state: UIControl.State) {
        _title = title
    }

//    @available(*, deprecated, message: "Please don't use this method, color only follow DSButton.Style")
//    public func setTitleColor(_ color: UIColor, for state: UIControl.State) {
//        titleLabel.textColor = color
//    }

    @available(*, deprecated, message: "Use setImage(_ image: UIImage?) instead")
    public func setImage(_ image: UIImage?, for _: UIControl.State) {
        _image = image?.color(style.normalForegroundColor)
    }

    /// The Value is closure trigger when button did tap
    public var didTap: (() -> Void)?

    // MARK: - Override

    override public var isHighlighted: Bool {
        didSet {
            guard isHighlighted else {
                if style.dimWhenHighlighted {
                    removeDimBlackLayer()
                }
                endBouncingEffect()
                return
            }
            backgroundColor = style.highlightedBackgroundColor
            imageView.image = _image?.color(style.highlightedForegroundColor)
            if style.dimWhenHighlighted {
                addDimBlackLayer()
            }
            beginBouncingEffect()
        }
    }

    override public var isEnabled: Bool {
        didSet {
            configureStyle()
        }
    }

    override public var intrinsicContentSize: CGSize {
        let minWidth = style.isTitleHidden ? style.buttonSize.height : style.buttonSize.minWidth
        var stackWidth: CGFloat = 0
        if !imageView.isHidden { stackWidth += imageView.intrinsicContentSize.width }
        if !style.isTitleHidden { stackWidth += titleLabel.intrinsicContentSize.width }
        if !loadingIndicator.isHidden { stackWidth += loadingIndicator.intrinsicContentSize.width }
        if !style.isTitleHidden && (!imageView.isHidden || !loadingIndicator.isHidden) { stackWidth += style.textPadding }
        let intrinsicWidth = !style.hasMargin || style.isTitleHidden ? stackWidth : stackWidth + 2 * defaultMargin

        let width = style.hasMargin ? max(intrinsicWidth, minWidth) : stackWidth
        return CGSize(width: width, height: style.buttonSize.height)
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }

    override public var state: UIControl.State { // super.state.rawValue |
        isLoading && isEnabled ? UIControl.State(rawValue: UIControl.State.application.rawValue) : super.state
    }

    // MARK: - Init

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        configureStyle()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
        configureStyle()
    }

    // MARK: - Setup view

    private var style: Style = Style.primary
    private let stackView = UIStackView()
    private let imageView = UIImageView(frame: .zero)
    private let loadingIndicator = UIActivityIndicatorView(style: .gray)
    private let titleLabel = UILabel()

    private var _title: String? {
        didSet {
            if state != .application {
                titleLabel.text = _title
            }
            invalidateIntrinsicContentSize()
            layout()
        }
    }

    private var _image: UIImage? {
        didSet {
            configureStyle()
            invalidateIntrinsicContentSize()
            layout()
        }
    }

    private func setupView() {
        addSubview(stackView)
        stackView.alignment = .center
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(loadingIndicator)
        stackView.addArrangedSubview(titleLabel)
        loadingIndicator.isHidden = true
        loadingIndicator.isUserInteractionEnabled = false
        imageView.isHidden = true
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center
        
        clipsToBounds = true
        
        stackView.isUserInteractionEnabled = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
            maker.leading.trailing.greaterThanOrEqualToSuperview().inset(defaultMargin).priority(.high)
        }
        titleLabel.setContentHuggingPriority(.defaultHigh + 1, for: .horizontal)
        titleLabel.setContentCompressionResistancePriority(.defaultHigh - 1, for: .horizontal)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.snp.makeConstraints { $0.width.height.equalTo(24) }

        addTarget(self, action: #selector(didHighLight), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(didUnHighLight), for: [.touchUpOutside, .touchUpInside, .touchCancel])
        addTarget(self, action: #selector(didTouchUpInside), for: .touchUpInside)
    }

    private func configureStyle() {

        stackView.spacing = style.textPadding
        titleLabel.font = style.titleSize.font
        titleLabel.text = style.isTitleHidden ? nil : _title
        titleLabel.isHidden = style.isTitleHidden

        switch state {
        case .normal:
            titleLabel.textColor = style.normalForegroundColor
            imageView.image = _image?.color(style.normalForegroundColor)
            backgroundColor = style.normalBackgroundColor
        case .disabled:
            titleLabel.textColor = style.disabledForegroundColor
            imageView.image = _image?.color(style.disabledForegroundColor)
            backgroundColor = style.disabledBackgroundColor
        case .highlighted:
            titleLabel.textColor = style.highlightedForegroundColor
        case .application:
            titleLabel.text = style.loadingTitle
            titleLabel.textColor = style.disabledForegroundColor
            imageView.image = _image?.color(style.disabledForegroundColor)
            backgroundColor = style.disabledBackgroundColor
        default:
            break
        }
        
        loadingIndicator.isHidden = !(isLoading && style.hasLoadingIndicator)
        imageView.isHidden = !loadingIndicator.isHidden || _image == nil

        layer.cornerRadius = style.cornerRadius

        let contentInset = style.contentPadding > 0 ? style.contentPadding : (!style.hasMargin || style.isTitleHidden ? 0 : defaultMargin)
        stackView.snp.updateConstraints { make in
            make.leading
                .trailing
                .greaterThanOrEqualToSuperview()
                .inset(contentInset)
                .priority(.high)
        }
    }

    private func updateLayout(_ isLoading: Bool, animated: Bool = true) {
        loadingIndicator.isHidden = !style.hasLoadingIndicator || !isLoading
        updateLayout(withLoading: style.hasLoadingIndicator && isLoading)
        invalidateIntrinsicContentSize()
        if animated {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: { [weak self] in
                self?.layoutIfNeeded()
            }, completion: nil)
        } else {
            layoutIfNeeded()
        }
    }

    private func updateLayout(withLoading isLoading: Bool) {
        isUserInteractionEnabled = !isLoading
        configAnimationLoading(isLoading)
        configureStyle()
        if isLoading {
            loadingIndicator.startAnimating()
            return
        }
        loadingIndicator.stopAnimating()
    }

    private func configAnimationLoading(_ isLoading: Bool) {
        if isLoading {
            loadingIndicator.isHidden = false
            imageView.isHidden = true
        } else {
            loadingIndicator.isHidden = true
            imageView.isHidden = _image != nil ? false : true
        }
    }

    @objc
    private func didHighLight() {
        isHighlighted = true
    }

    @objc
    private func didUnHighLight() {
        isHighlighted = false
    }

    @objc
    private func didTouchUpInside() {
        didTap?()
        configureStyle()
    }

    // MARK: - Layout

    private func layout() {
        imageView.contentMode = .scaleAspectFit
        stackView.removeArrangedSubview(imageView)
        stackView.removeArrangedSubview(loadingIndicator)
        if style.imagePosition == .right {
            stackView.insertArrangedSubview(imageView, at: stackView.arrangedSubviews.count)
            stackView.insertArrangedSubview(loadingIndicator, at: stackView.arrangedSubviews.count)
        } else {
            stackView.insertArrangedSubview(imageView, at: 0)
            stackView.insertArrangedSubview(loadingIndicator, at: 0)
        }

        loadingIndicator.tintColor = titleLabel.textColor
    }

}

private let defaultMargin: CGFloat = 16
