//
//  DSInfoView.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import UIKit
import Reusable
import RxCocoa
import RxSwift

class DSInfoView: UIView, NibOwnerLoadable {
    
    @IBOutlet weak private var mainContentWrapView: UIView!
    @IBOutlet weak private var contentWrapView: UIView!
    @IBOutlet weak private var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let didTapRelay = PublishRelay<Void>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }

    private func setupViews() {
        self.loadNibContent()
        
        mainContentWrapView.setShadowStyle(.shadow8)
        contentWrapView.layer.cornerRadius = contentWrapView.bounds.size.height / 2
        contentWrapView.layer.masksToBounds = true
        
        titleLabel.setStyle(DS.T14M(color: Colors.ink500))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc private func didTap() {
        didTapRelay.accept(())
    }
}

extension Reactive where Base: DSInfoView {
    var didTap: ControlEvent<Void> {
        return ControlEvent<Void>(events: base.didTapRelay)
    }
}

extension DSInfoView {
    
    
    @discardableResult
    func setLeftImage(_ image: UIImage) -> DSInfoView {
        leftImageView.image = image
        return self
    }
    
    @discardableResult
    func setTitle(_ title: String, titleColor: UIColor = Colors.ink400s) -> DSInfoView {
        
        titleLabel.textColor = titleColor
        titleLabel.text = title
        return self
    }
    
    @discardableResult
    func setBackgroundColor(_ color: UIColor = Colors.white500) -> DSInfoView {
        self.contentWrapView.backgroundColor = color
        return self
    }
}
