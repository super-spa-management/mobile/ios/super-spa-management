//
//  DSStatusLabel.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 3/4/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class DSStatusLabel: UIView, NibOwnerLoadable {
    
    struct Configuration {
        let style: Style
        let title: String
        let rightIcon: UIImage?
        let hasBorder: Bool
        
        init(style: Style, title: String, rightIcon: UIImage? = nil, hasBorder: Bool = true) {
            self.style = style
            self.title = title
            self.rightIcon = rightIcon
            self.hasBorder = hasBorder
        }
    }
    
    enum Style {
        case disabled
        case success
        case warning
        case error
        
        var backgroundColor: UIColor {
            switch self {
            case .disabled:
                return Colors.black100s
                
            case .success:
                return Colors.green300s
                
            case .warning:
                return Colors.orange400s
                
            case .error:
                return Colors.red300s
            }
        }
        
        var contentColor: UIColor {
            switch self {
            case .disabled:
                return Colors.ink400
                
            case .success:
                return Colors.green400
                
            case .warning:
                return Colors.orange400
                
            case .error:
                return Colors.red400
            }
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak private var rightIconImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    var didTap = PublishRelay<Void>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }

    private func setupViews() {
        self.loadNibContent()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapStatusLabel))
        self.addGestureRecognizer(tapGesture)

        rightIconImageView.backgroundColor = Colors.transparent
        
        containerView.layer.cornerRadius = 6.0
        containerView.layer.masksToBounds = true
        
        titleLabel.setStyle(DS.T14R())
    }
    
    func configure(_ configuration: Configuration) {
        
        rightIconImageView.image = configuration.rightIcon?.withRenderingMode(.alwaysTemplate).color(configuration.style.contentColor)
       
        titleLabel.text = configuration.title
        titleLabel.textColor = configuration.style.contentColor
        
        containerView.backgroundColor = configuration.style.backgroundColor
        
        if configuration.hasBorder {
            containerView.layer.borderWidth = 0.5
            containerView.layer.borderColor = Colors.black100s.cgColor
        }
    }
   
    @objc
    private func didTapStatusLabel() {
        didTap.accept(())
    }
}

extension Reactive where Base: DSStatusLabel {
    var didTap: ControlEvent<Void> {
        return ControlEvent<Void>(events: base.didTap)
    }
}
