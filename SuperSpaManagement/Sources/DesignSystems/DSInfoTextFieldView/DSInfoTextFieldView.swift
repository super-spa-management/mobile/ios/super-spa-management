//
//  DSInfoTextFieldView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class DSInfoTextFieldView: UIView, NibOwnerLoadable {
    
    enum Style {
        case smallest
        case medium
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var textViewContainerWrapView: UIView!
    @IBOutlet private  weak var contentTextView: UITextView!
    
    @IBOutlet private weak var textFieldContainerWrapView: UIView!
    @IBOutlet private weak var contentTextField: UITextField!
    
    private let disposeBag = DisposeBag()
    var didChangeContent = PublishRelay<String>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        
        self.loadNibContent()
        
        contentTextView.delegate = self
        contentTextField.delegate = self
        
        contentTextView.text = ""
        contentTextField.text = ""
        contentTextField.placeholder = "Nhập thông tin"
        
        [
            textViewContainerWrapView,
            textFieldContainerWrapView
        ]
        .forEach {
            $0?.layer.cornerRadius = 8.0
            $0?.layer.masksToBounds = true
            $0?.backgroundColor = Colors.backgroundDark
        }
        
        Observable
            .merge(
                contentTextField.rx.text.filterNil().filterEmpty(),
                contentTextView.rx.text.filterNil().filterEmpty()
            )
            .subscribeNext { [weak self] text in
                self?.didChangeContent.accept(text)
            }
            .disposed(by: disposeBag)
    }
    
}

extension DSInfoTextFieldView {
    @discardableResult
    func setTitle(_ title: String) -> DSInfoTextFieldView {
        titleLabel.text = title
        return self
    }
    
    @discardableResult
    func setStyle(_ style: Style) -> DSInfoTextFieldView {
        switch style {
        case .medium:
            textViewContainerWrapView.isHidden = false
            textFieldContainerWrapView.isHidden = true
            
        case .smallest:
            textViewContainerWrapView.isHidden = true
            textFieldContainerWrapView.isHidden = false
        }
        
        return self
    }
    
    @discardableResult
    func setText(_ text: String) -> DSInfoTextFieldView {
        if text.isNotEmpty {
            contentTextView.text = text
            contentTextField.text = text
        }
        
        return self
    }
}

extension DSInfoTextFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension DSInfoTextFieldView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension Reactive where Base: DSInfoTextFieldView {
    var didChangeContent: ControlEvent<String> {
        return ControlEvent(events: base.didChangeContent)
    }
}

