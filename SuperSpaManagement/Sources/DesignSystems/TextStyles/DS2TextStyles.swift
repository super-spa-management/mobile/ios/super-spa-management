// swiftlint:disable file_length

import Foundation
import UIKit

public protocol DS2TextStyle: DesignSystemTextStyle {
    var fontSize: CGFloat { get }
    var weight: UIFont.Weight { get }
    var normalColor: UIColor { get }
    var disableColor: UIColor { get } // Always has disable colors. No matter what Designer or BA tells you !!!
    var lineHeight: CGFloat { get }
}

// ************* 48 Money Unit *************
// -------- DARK -------
public struct DS2Dark48MoneyUnit: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light48MoneyUnit: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 0
    public init() {}
}

// ************* 48 Number *************
// -------- DARK -------
public struct DS2Dark48Number: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Dark48Number3: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light48Number: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light48Number3: DS2TextStyle {
    public let fontSize: CGFloat = 48.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// ************* 30 Money Unit *************
// -------- DARK -------
public struct DS2Dark30MoneyUnit: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 0
    public init() {}
}

public struct DS2Dark30MoneyUnit3: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 0.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light30MoneyUnit: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 0.0
    public init() {}
}

public struct DS2Light30MoneyUnit3: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 0.0
    public init() {}
}

// ************* 30 Hero *************
// -------- DARK -------
public struct DS2Dark30Hero: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light30Hero: DS2TextStyle {
    public let fontSize: CGFloat = 30.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// ************* 24 Headline *************
// -------- DARK -------
public struct DS2Dark24Headline: DS2TextStyle {
    public let fontSize: CGFloat = 24.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light24Headline: DS2TextStyle {
    public let fontSize: CGFloat = 24.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light24Headline3: DS2TextStyle {
    public let fontSize: CGFloat = 24.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light24Headline4: DS2TextStyle {
    public let fontSize: CGFloat = 24.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink4
    public let disableColor: UIColor = Colors.ds2Ink4
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light24HeadlineInfo: DS2TextStyle {
    public let fontSize: CGFloat = 24.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// ************* 20 Subhead *************
// -------- DARK -------
public struct DS2Dark20Subhead: DS2TextStyle {
    public let fontSize: CGFloat = 20.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light20Subhead: DS2TextStyle {
    public let fontSize: CGFloat = 20.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light20SubheadInfo: DS2TextStyle {
    public let fontSize: CGFloat = 20.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light20SubheadError: DS2TextStyle {
    public let fontSize: CGFloat = 20.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// ************* 16 Button *************
public struct DS2Light16ButtonSuccess: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Success1
    public let disableColor: UIColor = Colors.ds2Success1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16ButtonSuccess3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Success3
    public let disableColor: UIColor = Colors.ds2Success3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16ButtonError: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16ButtonError3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Error3
    public let disableColor: UIColor = Colors.ds2Error3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Button: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Button3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Informative3
    public let disableColor: UIColor = Colors.ds2Informative3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16ButtonMedium3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

// ************* 16 Body *************
// -------- DARK -------
public struct DS2Dark16BodyMedium: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark16BodyMedium3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark16Body: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark16Body2: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White2
    public let disableColor: UIColor = Colors.ds2White2
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark16Body3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light16BodyMedium: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Body: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Body2: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink2
    public let disableColor: UIColor = Colors.ds2Ink2
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Body3: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16Body4: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink4
    public let disableColor: UIColor = Colors.ds2Ink4
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16BodyInfo: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16BodySuccess: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Success1
    public let disableColor: UIColor = Colors.ds2Success1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16BodyWarning: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Warning1
    public let disableColor: UIColor = Colors.ds2Warning1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light16BodyError: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

// ************* 16 PARAGRAPH *************
// -------- DARK -------
public struct DS2Dark16ParagraphMedium: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Dark16Paragraph: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Dark16Paragraph2: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White2
    public let disableColor: UIColor = Colors.ds2White2
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light16ParagraphMedium: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light16Paragraph: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

public struct DS2Light16Paragraph2: DS2TextStyle {
    public let fontSize: CGFloat = 16.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink2
    public let disableColor: UIColor = Colors.ds2Ink2
    public let lineHeight: CGFloat = 8.0
    public init() {}
}

// ************* 14 Caption *************
// -------- DARK -------
public struct DS2Dark14CaptionMedium: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Dark14Caption: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Dark14Caption2: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White2
    public let disableColor: UIColor = Colors.ds2White2
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Dark14Caption3: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White3
    public let disableColor: UIColor = Colors.ds2White3
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Dark14CaptionMedium2: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White2
    public let disableColor: UIColor = Colors.ds2White2
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light14CaptionMedium: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionMedium2: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink2
    public let disableColor: UIColor = Colors.ds2Ink2
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionMedium3: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionMediumSuccess: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Success1
    public let disableColor: UIColor = Colors.ds2Success1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionMediumInfo: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14Caption: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14Caption2: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink2
    public let disableColor: UIColor = Colors.ds2Ink2
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14Caption3: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink3
    public let disableColor: UIColor = Colors.ds2Ink3
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionInfo: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionSuccess: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Success1
    public let disableColor: UIColor = Colors.ds2Success1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionWarning: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Warning1
    public let disableColor: UIColor = Colors.ds2Warning1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionError: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

public struct DS2Light14CaptionMediumError: DS2TextStyle {
    public let fontSize: CGFloat = 14.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 6.0
    public init() {}
}

// ************* 12 Caption Mini *************
// -------- DARK -------
public struct DS2Dark12CaptionMiniMedium: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark12CaptionMini: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White1
    public let disableColor: UIColor = Colors.ds2White1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Dark12CaptionMini2: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2White2
    public let disableColor: UIColor = Colors.ds2White2
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

// -------- LIGHT -------
public struct DS2Light12CaptionMiniMedium: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .medium
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light12CaptionMini: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink1
    public let disableColor: UIColor = Colors.ds2Ink1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light12CaptionMini2: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Ink2
    public let disableColor: UIColor = Colors.ds2Ink2
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light12CaptionMiniError: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Error1
    public let disableColor: UIColor = Colors.ds2Error1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}

public struct DS2Light12Button: DS2TextStyle {
    public let fontSize: CGFloat = 12.0
    public let weight: UIFont.Weight = .regular
    public let normalColor: UIColor = Colors.ds2Informative1
    public let disableColor: UIColor = Colors.ds2Informative1
    public let lineHeight: CGFloat = 4.0
    public init() {}
}
