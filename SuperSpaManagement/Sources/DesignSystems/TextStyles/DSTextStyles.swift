//
//  DSTextStyles.swift
//  DesignSystem
//
//  Created by TAMPT12 on 3/18/20.
//

import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command type_name

public protocol DesignSystemTextStyle {
    var fontSize: CGFloat { get }
    var weight: UIFont.Weight { get }
    var lineHeight: CGFloat { get }
}

extension DesignSystemTextStyle {
    public var font: UIFont { UIFont.systemFont(ofSize: fontSize, weight: weight) }
}

public protocol DSTextStyle: DesignSystemTextStyle {
    var fontSize: CGFloat { get }
    var weight: UIFont.Weight { get }
    var color: UIColor { get }
    var lineHeight: CGFloat { get }
}

public enum DS {

    public struct T48R: DSTextStyle {
        public let fontSize: CGFloat = 48.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 56
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T48M: DSTextStyle {
        public let fontSize: CGFloat = 48.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 56
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T30R: DSTextStyle {
        public let fontSize: CGFloat = 30.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 38
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T30M: DSTextStyle {
        public let fontSize: CGFloat = 30.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 38
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T24R: DSTextStyle {
        public let fontSize: CGFloat = 24.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 32
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T24M: DSTextStyle {
        public let fontSize: CGFloat = 24.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 32
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T20R: DSTextStyle {
        public let fontSize: CGFloat = 20.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T20M: DSTextStyle {
        public let fontSize: CGFloat = 20.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16R: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 24
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16M: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 24
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16R20: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16M20: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T14R: DSTextStyle {
        public let fontSize: CGFloat = 14.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T14M: DSTextStyle {
        public let fontSize: CGFloat = 14.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T12R: DSTextStyle {
        public let fontSize: CGFloat = 12.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 16
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T12M: DSTextStyle {
        public let fontSize: CGFloat = 12.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 16
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T10R: DSTextStyle {
        public let fontSize: CGFloat = 10.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 12
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T10M: DSTextStyle {
        public let fontSize: CGFloat = 10.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 12
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
}

// swiftlint:enable type_name
