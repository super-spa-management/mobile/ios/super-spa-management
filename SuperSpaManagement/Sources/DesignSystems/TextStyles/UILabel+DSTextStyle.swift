//
//  UILabel+DSTextStyle.swift
//  DesignSystem
//
//  Created by Dao Duy Quang on 9/18/19.
//  Copyright © 2019 Dao Duy Quang. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {

    public func setStyle(_ style: DSTextStyle) {
        self.font = UIFont.systemFont(ofSize: style.fontSize, weight: style.weight)
        self.textColor = style.color
    }
}

extension UILabel {

    func setAttributedText(_ attributedText: NSAttributedString?, lineHeight: CGFloat) {
        guard let attributedString = attributedText else {
            self.attributedText = nil
            return
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeight > font.lineHeight ? lineHeight / font.lineHeight : 0
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineBreakMode = lineBreakMode
        let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
        mutableAttributedString.addAttribute(.paragraphStyle,
                                             value: paragraphStyle,
                                             range: NSRange(location: 0, length: attributedString.length))
        self.attributedText = mutableAttributedString
    }

    func setText(_ text: String?, isStrikethrough: Bool) {
        setText(text, attribute: TextAtribute(lineHeight: font.lineHeight,
                                              strikethroughStyle: isStrikethrough ? .thick : nil))
    }

    private func setText(_ text: String?, attribute: TextAtribute) {
        guard let text = text else {
            self.text = nil
            return
        }
        let paragraphStyle = NSMutableParagraphStyle()
        var baselineOffset: CGFloat = 0
 
            // Line height multiple
            let estimatedLineHeight = 0.75 * attribute.lineHeight + 0.25 * font.lineHeight
            paragraphStyle.lineHeightMultiple = estimatedLineHeight / font.lineHeight
            baselineOffset = (estimatedLineHeight - font.lineHeight) / 2.0
        // Alignment
        paragraphStyle.alignment = attribute.alignment ?? textAlignment
        // Line Break
        paragraphStyle.lineBreakMode = lineBreakMode

        var attributes = [NSAttributedString.Key: Any]()
        // Font
        if let style = attribute.style {
            attributes[.font] = style.font
            if let style = style as? DS2TextStyle {
                attributes[.foregroundColor] = style.normalColor
            } else if let style = style as? DSTextStyle {
                attributes[.foregroundColor] = style.color
            }
        }
        attributes[.paragraphStyle] = paragraphStyle
        attributes[.baselineOffset] = baselineOffset
        attributes[.strikethroughStyle] = attribute.strikethroughStyle?.rawValue
        attributes[.underlineStyle] = attribute.underlineStyle?.rawValue
        self.attributedText = NSAttributedString(string: text, attributes: attributes)
    }
}

private struct TextAtribute {

    let style: DesignSystemTextStyle?
    let lineHeight: CGFloat
    let alignment: NSTextAlignment?
    let strikethroughStyle: NSUnderlineStyle?
    let underlineStyle: NSUnderlineStyle?

    init(style: DesignSystemTextStyle? = nil,
         lineHeight: CGFloat,
         alignment: NSTextAlignment? = nil,
         strikethroughStyle: NSUnderlineStyle? = nil,
         underlineStyle: NSUnderlineStyle? = nil) {

        self.style = style
        self.lineHeight = lineHeight
        self.alignment = alignment
        self.strikethroughStyle = strikethroughStyle
        self.underlineStyle = underlineStyle
    }
}
