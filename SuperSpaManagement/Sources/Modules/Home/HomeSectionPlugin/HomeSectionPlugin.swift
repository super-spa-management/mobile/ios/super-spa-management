//
//  HomeSectionPlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

protocol HomeSectionPlugin: AnyObject {
    
    var listener: HomeSectionPluginListener? { get set }
    var extraData: [String: Any] { get  set }
    func build(listener: HomeSectionPluginListener) -> BaseWireframe
}

extension HomeSectionPlugin {
    var id: Int {
        return ObjectIdentifier(self).hashValue
    }
}

enum HomeSectionCodeType: String {
    case none = ""
    case bannerSection = "beepe_banner_section"
    case circleCarouselSection = "beepe_circle_carousel_section"
    case mediumCarouselSection = "beepe_medium_carousel_section"
    case largeCarouselSection = "beepe_large_carousel_section"
    case largeSingleCarouselSection = "beepe_large_single_carousel_section"
    case gridViewSection = "beepe_grid_view_section"
    case singleViewSection = "SuperSpaManagement_single_view_section"
}
enum HomeSectionPlugins {
    static var registeredPluginsMapping: [HomeSectionCodeType: HomeSectionPlugin] {
        return [
            .bannerSection : BannerTemplatePlugin(),
            .circleCarouselSection: CircleCarouselTemplatePlugin(),
            .mediumCarouselSection: MediumCarouselTemplatePlugin(),
            .largeCarouselSection: LargeCarouselTemplatePlugin(),
            .largeSingleCarouselSection: LargeSingleCarouselTemplatePlugin(),
            .gridViewSection: GridViewTemplatePlugin()
        ]
    }
}
