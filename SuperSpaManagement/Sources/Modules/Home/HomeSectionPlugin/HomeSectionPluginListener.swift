//
//  HomeSectionPluginListener.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

/// Plugin defination
protocol HomeSectionPluginListener: AnyObject {
    func showPlugin(_ plugin: HomeSectionPlugin)
    func hidePlugin(_ plugin: HomeSectionPlugin)
    func detachPlugin(_ plugin: HomeSectionPlugin)
    func reloadPlugin(_ plugin: HomeSectionPlugin)
}
