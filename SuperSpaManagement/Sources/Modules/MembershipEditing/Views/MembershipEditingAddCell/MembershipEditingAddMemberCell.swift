//
//  MembershipEditingAddMemberCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Reusable
import RxSwift

class MembershipEditingAddMemberCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var addMembershipButton: DSButton!
    
    private let disposeBag = DisposeBag()
    var onAddMembership: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        addMembershipButton.setStyle(.primaryDestruction)
        
        addMembershipButton
            .rx
            .tap
            .subscribeNext { [weak self] in
                self?.onAddMembership?()
            }
            .disposed(by: disposeBag)
        
    }
    
    func configureCell(title: String) {
        addMembershipButton.setTitle(title)
    }
}
