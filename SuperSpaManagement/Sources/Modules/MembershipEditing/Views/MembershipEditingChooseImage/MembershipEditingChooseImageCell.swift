//
//  MembershipEditingChooseImageCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Reusable
import RxSwift

class MembershipEditingChooseImageCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var chooseImageView: DSChooseImageView!
    private let disposeBag = DisposeBag()
    
    var didChangeImage: ((UIImage) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        chooseImageView
            .rx
            .didChangeImage
            .subscribeNext { [weak self] image in
                self?.didChangeImage?(image)
            }
            .disposed(by: disposeBag)
    }
    
    func configureCell(title: String, presentViewController: UIViewController, initAssetId: String)  {
        chooseImageView
            .setTitle(title)
            .setPresentViewController(presentViewController)
            .setImage(by: initAssetId)
    }
    
}
