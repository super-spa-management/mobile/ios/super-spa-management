//
//  MembershipEditingInfoCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Reusable
import RxSwift

class MembershipEditingInfoCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var infoView: DSInfoTextFieldView!
    
    private let disposeBag = DisposeBag()
    var didChangeContent: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        infoView
            .rx
            .didChangeContent
            .subscribeNext { [weak self] content in
                self?.didChangeContent?(content)
            }
            .disposed(by: disposeBag)
    }
    func configureCell(title: String, style: DSInfoTextFieldView.Style, initContent: String)  {
        infoView.setTitle(title)
            .setStyle(style)
            .setText(initContent)
    }
}
