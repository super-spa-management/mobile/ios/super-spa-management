//
//  MembershipItemCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/22/21.
//

import UIKit
import Reusable
import Differentiator
import RxSwift
import RxCocoa

struct MembershipItemViewModel {
    let model: Membership
    
    var name: String {
        return model.name
    }
    
    var phoneNumber: String {
        return model.phoneNumber
    }
    
    var avatar: Observable<UIImage?> {
        return PhotoReader.load(savedAssetId: model.avatarSavedAssetId)
    }
}

extension MembershipItemViewModel: IdentifiableType {
    typealias Identity = Int
    var identity: Int {
        return model.id
    }
}

extension MembershipItemViewModel: Equatable {
    static func == (lhs: MembershipItemViewModel, rhs: MembershipItemViewModel) -> Bool {
        return lhs.identity == lhs.identity
            && lhs.model.avatarSavedAssetId == rhs.model.avatarSavedAssetId
            && lhs.name == rhs.name
            && lhs.phoneNumber == rhs.phoneNumber
    }
}

class MembershipItemCell: UITableViewCell, NibReusable {

    @IBOutlet weak var avatarContainerView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var membershipNameLabel: UILabel!
    @IBOutlet weak var membershipPhoneNumberLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarContainerView.layer.cornerRadius = 8.0
        avatarContainerView.layer.masksToBounds = true
        avatarContainerView.backgroundColor = Colors.green300
        
        avatarContainerView.layer.borderWidth = 0.15
        avatarContainerView.layer.borderColor = Colors.ink400s.cgColor
        
        membershipNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        membershipPhoneNumberLabel.setStyle(DS.T12R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: MembershipItemViewModel) {
        membershipNameLabel.text = viewModel.name
        membershipPhoneNumberLabel.text = viewModel.phoneNumber
        
        viewModel
            .avatar
            .asDriver(onErrorDriveWith: .never())
            .driveNext { [weak self] image in
                self?.avatarImageView.image = image
            }
            .disposed(by: disposeBag)
    }
}
