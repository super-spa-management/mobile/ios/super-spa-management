//
//  ProductInfoEditingViewController.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class ProductInfoEditingViewController: UIViewController {

    // MARK: - Public properties -

    var presenter: ProductInfoEditingPresenterInterface!

    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// MARK: - Extensions -

extension ProductInfoEditingViewController: ProductInfoEditingViewInterface {
}
