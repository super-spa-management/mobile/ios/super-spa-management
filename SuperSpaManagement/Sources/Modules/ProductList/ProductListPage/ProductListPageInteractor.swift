//
//  ProductListPageInteractor.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import RxSwift
protocol ProductListPageInteractorInterface: InteractorInterface {
    func getProducts() -> Observable<[Product]>
}

final class ProductListPageInteractor {
}

// MARK: - Extensions -

extension ProductListPageInteractor: ProductListPageInteractorInterface {
    func getProducts() -> Observable<[Product]> {
        return Observable.of(
            Array(
                repeating: 0,
                count: 24
            )
            .indices
            .map {
                Product(id: $0, name: "product test \($0)", productDescription: "Product description test \($0)", price: 30000 * $0, savedAssetId: "", category: nil, totalItem: $0 * 10)
            }
        )
    }
}
