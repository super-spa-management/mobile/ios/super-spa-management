//
//  ProductItemCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Differentiator
import Reusable


extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        Formatter.withSeparator.string(for: self).or("")
    }
}

struct ProductItemViewModel {
    let model: Product
    
    init(model: Product) {
        self.model = model
    }
}

extension ProductItemViewModel: Equatable {
    static func == (lhs: ProductItemViewModel, rhs: ProductItemViewModel) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension ProductItemViewModel: IdentifiableType {
    typealias Identity = Int
    var identity: Int {
        return model.id
    }
}

extension ProductItemViewModel {
    
    var productName: String {
        return model.name
    }
    
    var productInfo: String {
        return model.productDescription
    }
    
    var productPriceFormatted: String {
        if model.price == .zero {
            return "Miễn phí"
        }
        return model.price.formattedWithSeparator + " VND"
    }
}


class ProductItemCell: UITableViewCell, NibReusable {

    @IBOutlet weak private var dividerView: UIView!
    @IBOutlet weak private var imageViewContainerView: UIView!
    
    @IBOutlet weak private var productNameLabel: UILabel!
    @IBOutlet weak private var productInfoLabel: UILabel!
    @IBOutlet private weak var productPriceValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        dividerView.backgroundColor = Colors.blue200
        
        imageViewContainerView.layer.cornerRadius = 8.0
        imageViewContainerView.layer.masksToBounds = true
        
        productNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        productInfoLabel.setStyle(DS.T12R(color: Colors.ink400s))
        
        productPriceValueLabel.setStyle(DS.T14R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: ProductItemViewModel) {
        productNameLabel.text = viewModel.productName
        productInfoLabel.text = viewModel.productInfo
        productPriceValueLabel.text = viewModel.productPriceFormatted
    }
}
