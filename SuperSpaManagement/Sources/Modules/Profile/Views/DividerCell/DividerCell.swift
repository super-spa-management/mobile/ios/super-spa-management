//
//  DividerCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/14/21.
//

import UIKit
import Reusable

class DividerCell: UITableViewCell, NibReusable {

    @IBOutlet weak var dividerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dividerView.backgroundColor = Colors.ink200s
    }
}
