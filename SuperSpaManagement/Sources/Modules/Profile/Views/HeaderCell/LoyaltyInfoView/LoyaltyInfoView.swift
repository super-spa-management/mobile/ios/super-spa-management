//
//  LoyaltyInfoView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/14/21.
//

import Foundation
import Reusable

class LoyaltyInfoView: UIView, NibOwnerLoadable {
    
    @IBOutlet private weak var iconWrapView: UIView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        
        iconWrapView.layer.cornerRadius = 16.0
        titleLabel.setStyle(DS.T10R(color: Colors.ink400s))
        titleLabel.text = "Default"
        
        valueLabel.setStyle(DS.T14M(color: Colors.ink500))
        valueLabel.text = "0"
    }
}

extension LoyaltyInfoView {
    @discardableResult
    func setTitle(_ title: String) -> LoyaltyInfoView {
        titleLabel.text = title
        return self
    }
    
    @discardableResult
    func setValue(_ value: Int) -> LoyaltyInfoView {
        valueLabel.text = "\(value)"
        return self
    }
    
    @discardableResult
    func setIcon(_ icon: UIImage) -> LoyaltyInfoView {
        iconImageView.image = icon
        return self
    }
}
