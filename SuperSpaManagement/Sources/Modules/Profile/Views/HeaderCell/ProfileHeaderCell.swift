//
//  ProfileHeaderCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/10/21.
//

import UIKit
import Reusable
import Differentiator
import RxSwift
import RxCocoa


struct ProfileHeaderViewModel {
    let model: Membership
    
    var shortcutName: String {
        return String(model.name.first.or(Character("B")))
    }
    
    var userName: String {
        model.name
    }
}

extension ProfileHeaderViewModel: Equatable {
    static func == (lhs: ProfileHeaderViewModel, rhs: ProfileHeaderViewModel) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension ProfileHeaderViewModel: IdentifiableType {
    typealias Identity = Int
    var identity: Int {
        return model.id
    }
}

class ProfileHeaderCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var profileInfoContainerWrapView: UIView!
    @IBOutlet private weak var profileInfoContainerView: UIView!
    
    @IBOutlet private weak var profileTierContainerView: UIView!
    
    @IBOutlet private weak var profileInfoStackView: UIStackView!
    @IBOutlet private weak var avatarWrapView: UIView!
    @IBOutlet private weak var shortcutNameLabel: UILabel!
    @IBOutlet private weak var userNameLabel: UILabel!
    
    @IBOutlet private weak var userLoytalInfoStackView: UIStackView!
    
    @IBOutlet private weak var loyaltyGemsInfoView: LoyaltyInfoView!
    @IBOutlet private weak var loyaltyPointsInfoView: LoyaltyInfoView!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        avatarWrapView.layer.cornerRadius = avatarWrapView.frame.height / 2
        avatarWrapView.layer.masksToBounds = true
    }
    
    private func setupViews() {
        profileInfoContainerWrapView.setShadowStyle(.shadow8)
        
        profileInfoContainerView.layer.cornerRadius = 8.0
        profileInfoContainerView.layer.masksToBounds = true
        profileInfoContainerView.layer.borderWidth = 0.1
        profileInfoContainerView.layer.borderColor = Colors.ink300s.cgColor
        
        profileTierContainerView.backgroundColor = Colors.yellow
        profileTierContainerView.layer.cornerRadius = 8.0
        profileTierContainerView.layer.masksToBounds = true
        
        profileInfoStackView.backgroundColor = Colors.ink200s
        
        avatarWrapView.backgroundColor = Colors.red500
        shortcutNameLabel.setStyle(DS.T24M(color: Colors.white500))
        
        userNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        
        userLoytalInfoStackView.backgroundColor = Colors.ink200s
        
        loyaltyGemsInfoView
            .setTitle("Gems")
            .setValue(100)
            .setIcon(Asset.Icon.Common.icIosSystemStar.image.color(Colors.white500))
        
        loyaltyPointsInfoView.setTitle("Points").setValue(999)
    }
    
    func configureCell(viewModel: ProfileHeaderViewModel) {
        shortcutNameLabel.text = viewModel.shortcutName
        userNameLabel.text = viewModel.userName
    }
}
