//
//  ProfileInfoCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/14/21.
//

import UIKit
import Reusable

struct ProfileInfoViewModel {
    let title: String
    let icon: UIImage
}

class ProfileInfoCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var leftIconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        titleLabel.setStyle(DS.T14M(color: Colors.ink500))
    }
    
    func configureCell(viewModel: ProfileInfoViewModel) {
        titleLabel.text = viewModel.title
        leftIconImageView.image = viewModel.icon
    }
}
