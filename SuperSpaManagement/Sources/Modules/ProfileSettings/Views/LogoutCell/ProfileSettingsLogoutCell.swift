//
//  ProfileSettingsLogoutCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/12/21.
//

import UIKit
import Reusable

class ProfileSettingsLogoutCell: UITableViewCell, NibReusable {

    @IBOutlet weak var logoutTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        logoutTitleLabel.setStyle(DS.T16R(color: Colors.red400))
        logoutTitleLabel.text = "Đăng xuất tài khoản"
    }
}
