//
//  BannerTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class BannerTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return BannerTemplateWireframe()
    }
}
