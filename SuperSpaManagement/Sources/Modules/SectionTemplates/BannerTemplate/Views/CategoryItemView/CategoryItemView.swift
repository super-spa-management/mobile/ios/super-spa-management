//
//  CategoryItemView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import UIKit
import Reusable

struct CategoryItem {
    let containerBackgroundColor: UIColor
    let title: String
}

class CategoryItemView: UIView, NibOwnerLoadable {
    
    @IBOutlet private weak var iconContainerWrapView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        iconContainerWrapView.layer.cornerRadius = iconContainerWrapView.bounds.width / 2
        iconContainerWrapView.layer.masksToBounds = true
    }

    private func setupViews() {
        self.loadNibContent()
        titleLabel.setStyle(DS.T10R(color: Colors.ink400))
    }
}

extension CategoryItemView {
    
    @discardableResult
    func setIconContainerBackgroundColor(_ color: UIColor) -> CategoryItemView {
        iconContainerWrapView.backgroundColor = color
        return self
    }
    
    @discardableResult
    func setCategoryTitle(_ title: String) -> CategoryItemView {
        titleLabel.text = title
        return self
    }
}
