//
//  SuperSpaManagementPagerView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import UIKit
import Reusable
import FSPagerView

struct SSMBannerPageItem {
    let backggroundColor: UIColor
}

private let pageBackgroundColors: [UIColor] = [
    Colors.yellow.withAlphaComponent(0.7),
    Colors.green300,
    Colors.ds2Warning1.withAlphaComponent(0.8),
    Colors.red300
]

class SSMPagerView: UIView, NibOwnerLoadable {
    private let pagerItems: [SSMBannerPageItem] = Array(
        repeating: 0,
        count: 5
    )
    .indices
    .map {
        SSMBannerPageItem(
            backggroundColor: pageBackgroundColors[$0 % pageBackgroundColors.count]
        )
    }
    
    @IBOutlet weak var pagerView: FSPagerView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    private func setupViews() {
        self.loadNibContent()
        
//        pagerView.transformer = FSPagerViewTransformer(type: .crossFading)
        pagerView.automaticSlidingInterval = 3.0
        pagerView.isInfinite = true
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "SuperSpaManagement_banner_pager_cell_identifier")
        
        pagerView.dataSource = self
        pagerView.delegate = self
    }
}

extension SSMPagerView: FSPagerViewDelegate {
    
}

extension SSMPagerView: FSPagerViewDataSource {
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "SuperSpaManagement_banner_pager_cell_identifier", at: index)
        cell.backgroundColor = pagerItems[index].backggroundColor
        return cell
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return pagerItems.count
    }
}
