//
//  SmallCarouselTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class MediumCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .medium)
    }
}

class LargeCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .large)
    }
}

class LargeSingleCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .largeSingle)
    }
}
