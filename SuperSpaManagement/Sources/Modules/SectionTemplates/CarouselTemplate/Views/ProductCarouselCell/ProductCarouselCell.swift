//
//  ProductCarouselCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import UIKit
import Reusable

protocol CarouselItemable {
    var id: Int { get set }
    var title: String { get set }
    var moreInfo: String { get set }
}

struct ProductCarouselCellViewModel {
    
}

class ProductCarouselCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var productImageContainerView: UIView!
    @IBOutlet private weak var productNameLabel: UILabel!
    
    @IBOutlet private weak var moreInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        productImageContainerView.layer.cornerRadius = 8.0
        productImageContainerView.layer.masksToBounds = true
        
        productNameLabel.setStyle(DS.T12M(color: Colors.ink500))
        moreInfoLabel.setStyle(DS.T12R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: ProductCarouselCellViewModel, style: CarouselTemplateStyle) {
        switch  style {
        case .medium:
            moreInfoLabel.isHidden = true
        case .large, .largeSingle:
            moreInfoLabel.isHidden = false
        }
    }
}
