//
//  CircleCarouselTemplatePresenter.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import RxSwift
import RxCocoa
import Action

protocol CircleCarouselTemplateViewInterface: ViewInterface {
    var viewModels: BehaviorRelay<[CircleCarouselCellViewModel]> { get }
}

protocol CircleCarouselTemplateListener: AnyObject {
    func circleCarouselTemplateDidSelected(_ model: CircleCarouselItemable)
    func cirlceCarouselTemplateDidGetErorr(_ error: SuperSpaManagementError)
    func circleCarouselTemplateDidGetEmptyData()
    func cirlceCaroulseTemplateGotData()
}

final class CircleCarouselTemplatePresenter: CircleCarouselTemplatePresenterInterface {
    
    let selectedItemtrigger = PublishRelay<CircleCarouselItemable>()
    // MARK: - Private properties -
    private lazy var getItemsAction = makeGetItemsAction()
    private let disposeBag = DisposeBag()
    
    private unowned let view: CircleCarouselTemplateViewInterface
    private let listener: CircleCarouselTemplateListener
    private let interactor: CircleCarouselTemplateInteractorInterface
    private let wireframe: CircleCarouselTemplateWireframeInterface
    
    // MARK: - Lifecycle -
    
    init(view: CircleCarouselTemplateViewInterface,
         interactor: CircleCarouselTemplateInteractorInterface,
         wireframe: CircleCarouselTemplateWireframeInterface,
         listener: CircleCarouselTemplateListener) {
        
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.listener = listener
        
        configurePresenter()
        configureViewActions()
    }
}

// MARK: - Extensions -

extension CircleCarouselTemplatePresenter {
    private func configureViewActions() {
        selectedItemtrigger
            .subscribeNext { [weak listener] selectedItem in
                listener?.circleCarouselTemplateDidSelected(selectedItem)
            }
            .disposed(by: disposeBag)
    }
}



extension CircleCarouselTemplatePresenter {
    private func configurePresenter() {
        getItemsAction
            .elements
            .filterEmpty()
            .map { items -> [CircleCarouselCellViewModel] in
                return items.map { CircleCarouselCellViewModel(model: $0) }
            }
            .bind(to: view.viewModels)
            .disposed(by: disposeBag)
        
        getItemsAction
            .elements
            .filterEmpty()
            .subscribeNext { [weak listener]_ in
                listener?.circleCarouselTemplateDidGetEmptyData()
            }
            .disposed(by: disposeBag)
        
        getItemsAction
            .underlyingError
            .map { SuperSpaManagementError(error: $0) }
            .subscribeNext { [weak listener]error in
                listener?.cirlceCarouselTemplateDidGetErorr(error)
            }
            .disposed(by: disposeBag)
        
        getItemsAction.execute(.zero)
    }
}

extension CircleCarouselTemplatePresenter {
    func makeGetItemsAction() -> Action<Int, [CircleCategoryItem]> {
        return Action<Int, [CircleCategoryItem]> { [unowned  self] categoryId in
            return interactor.getItems(by: categoryId)
        }
    }
}
