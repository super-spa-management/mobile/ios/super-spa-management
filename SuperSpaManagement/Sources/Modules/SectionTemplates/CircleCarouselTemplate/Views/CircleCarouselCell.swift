//
//  CircleCarouselCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import UIKit
import Reusable

protocol CircleCarouselItemable {
    var id: Int { get set }
    var title: String { get set }
    var moreInfo: String { get set }
    var iconUrl: String? { get set }
}

struct CircleCategoryItem: CircleCarouselItemable {
    var id: Int = 0
    
    var title: String = ""
    
    var moreInfo: String = ""
    
    var iconUrl: String? = ""
}

struct CircleCarouselCellViewModel {
    let model: CircleCarouselItemable
    init(model: CircleCarouselItemable) {
        self.model = model
    }
    
    var title: String {
        return model.title
    }
}

class CircleCarouselCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var iconContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        iconContainerView.layer.cornerRadius = 32.0
        iconContainerView.layer.masksToBounds = true
        iconContainerView.backgroundColor = Colors.green300
        titleLabel.setStyle(DS.T14M(color: Colors.ink500))
    }
    
    func configureCell(viewModel: CircleCarouselCellViewModel) {
        titleLabel.text = viewModel.title
    }
}
