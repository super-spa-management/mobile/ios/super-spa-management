//
//  OrderItemCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/23/21.
//

import UIKit
import Reusable

struct OrderItemViewModel {
    let model: Product
}

class OrderItemCell: UITableViewCell, NibReusable {

    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        
    }
    
    func configureCell(viewModel: OrderItemViewModel) {}
}
