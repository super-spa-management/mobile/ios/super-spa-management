//
//  BeepbeeNatrium.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/7/21.
//

import Foundation

fileprivate enum AppEnvironment {
    case dev
    case staging
    case uat
    case production
}

fileprivate protocol NatriumMappable {
    var evironmentParameters: [AppEnvironment: String] { get }
}

fileprivate enum ApiBase: NatriumMappable {
    case `current`
    
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev : "http://localhost:3333",
            .staging: "http://SuperSpaManagement.com",
            .production: "http://SuperSpaManagement.com"
        ]
    }
    
    var value: String {
        return (self.evironmentParameters[SSMAppEnvironment.current.env]).or("")
    }
}


fileprivate class SSMAppEnvironment {
    public static let current = SSMAppEnvironment()
    
    var env: AppEnvironment { return .dev }
}

enum SSMNatrium {
    public static let apiBase: String = ApiBase.current.value
}
