//
//  MembershipServices.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/28/21.
//


import Foundation
import RealmSwift
import RxSwift
import Resolver

protocol MembershipsServiceType {
    func create(_ membership: Membership) -> Single<Void>
    func update(_ membership: Membership) -> Single<Void>
    
    func getMemberships() -> Observable<[Membership]>
}

class MembershipsService: MembershipsServiceType {
    
    @Injected var realmManager: RealmManagerType
    
    func create(_ membership: Membership) -> Single<Void> {
        return createOrUpdate(membership, update: false)
    }
    
    func update(_ membership: Membership) -> Single<Void> {
        return createOrUpdate(membership, update: true)
    }
    
    func getMemberships() -> Observable<[Membership]> {
        return Observable
            .just(
                realmManager
                    .getAll(CodableMembershipObject.self)
                    .toArray(type: CodableMembershipObject.self)
                    .map { $0.asMembership() }
            )
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    private func createOrUpdate(_ membership: Membership, update: Bool) -> Single<Void> {
        return realmManager.create(object: membership.asCodableObject(), update: update) {  [weak self] object in
            guard let self = self else { return }
            object.id = self.realmManager.incrementID(type: CodableMembershipObject.self)
        }
        .observe(on: MainScheduler.instance)
    }
}
