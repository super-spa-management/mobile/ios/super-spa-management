//
//  Gender.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import Foundation

enum Gender: Int {
    case male = 0
    case female = 1
    case other = 2
}
