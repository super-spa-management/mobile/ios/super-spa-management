//
//  Membership.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import Foundation
import RealmSwift

struct Membership {
    var id: Int
    var name: String
    var gender: Gender
    var phoneNumber: String
    var avatarSavedAssetId: String
    var address: String
    
    public init(id: Int = -1,
                name: String = "Unknowned",
                gender: Gender = .female,
                phoneNumber: String = "08xxxxxxxxxx",
                avatarSavedAssetId: String = "",
                address: String = "") {
        self.id = id
        self.name = name
        self.gender = gender
        self.gender = gender
        self.phoneNumber = phoneNumber
        self.avatarSavedAssetId = avatarSavedAssetId
        self.address = address
    }
    
    static var `default`: Membership = Membership()
}

extension Membership {
    func asCodableObject() -> CodableMembershipObject {
        return CodableMembershipObject(
            id: id,
            name: name,
            gender: gender.rawValue,
            phoneNumber: phoneNumber,
            avatarSavedAssetId: avatarSavedAssetId,
            address: address
        )
    }
}

@objcMembers class CodableMembershipObject: Object, Decodable {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var gender = 0
    dynamic var phoneNumber: String = ""
    dynamic var avatarSavedAssetId: String = ""
    dynamic var address: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id, name, address, gender
        case phoneNumber = "phone_number"
        case avatarSavedAssetId = "avatar_saved_asset_id"
    }
    
    override init() {
        super.init()
    }
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    init(id: Int = 0, name: String = "", gender: Int = 0, phoneNumber: String = "", avatarSavedAssetId: String = "", address: String = "") {
        self.id = id
        self.name = name
        self.gender = gender
        self.phoneNumber = phoneNumber
        self.avatarSavedAssetId = avatarSavedAssetId
        self.address = address
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        gender = try container.decode(Int.self, forKey: .gender)
        phoneNumber = try container.decode(String.self, forKey: .phoneNumber)
        avatarSavedAssetId = try container.decode(String.self, forKey: .avatarSavedAssetId)
        address = try container.decode(String.self, forKey: .address)
    }
    
    func asMembership() -> Membership {
        return Membership(
            id: id,
            name: name,
            gender: Gender(rawValue: gender).or(.female),
            phoneNumber: phoneNumber,
            avatarSavedAssetId: avatarSavedAssetId,
            address: address
        )
    }
}
