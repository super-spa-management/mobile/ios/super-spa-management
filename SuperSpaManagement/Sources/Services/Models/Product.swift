//
//  Product.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/22/21.
//

import Foundation
import RealmSwift
import Differentiator

struct Category {
    let id: Int
    let name: String
}

extension Category {
    func asCodableObject() -> CodableCategoryObject {
        return CodableCategoryObject(
            id: id,
            name: name
        )
    }
}

@objcMembers class CodableCategoryObject: Object, Decodable {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case id, name
    }
    
    
    override init() {
        super.init()
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
    }
    
    func asCategory() -> Category {
        return Category(
            id: id,
            name: name
        )
    }
}

struct Product {
    let id: Int
    let name: String
    let productDescription: String
    let price: Int
    let savedAssetId: String
    let category: Category?
    let totalItem: Int
}

extension Product {
    func asCodableObject() -> CodableProductObject {
        return CodableProductObject(
            id: id,
            name: name,
            productDescription: productDescription,
            price: price,
            savedAssetId: savedAssetId,
            realmCategory: category?.asCodableObject(),
            totalItem: totalItem
        )
    }
}

@objcMembers class CodableProductObject: Object, Decodable {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var productDescription: String = ""
    dynamic var price: Int = 0
    dynamic var savedAssetId: String = ""
    dynamic var realmCategory: CodableCategoryObject? = nil
    dynamic var totalItem: Int = 0
    
    
    override init() {
        super.init()
    }
    
    init(id: Int,
         name: String,
         productDescription: String,
         price: Int,
         savedAssetId: String,
         realmCategory: CodableCategoryObject?,
         totalItem: Int) {
        self.id = id
        self.name = name
        self.productDescription = productDescription
        self.price = price
        self.savedAssetId = savedAssetId
        self.realmCategory = realmCategory
        self.totalItem = totalItem
        
    }
    
    private enum CodingKeys: String, CodingKey {
        case id,name,price,category
        case productDescription = "product_description"
        case savedAssetId = "saved_assetId"
        case totalItem = "total_item"
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        productDescription = try container.decode(String.self, forKey: .productDescription)
        price = try container.decode(Int.self, forKey: .price)
        savedAssetId = try container.decode(String.self, forKey: .savedAssetId)
        realmCategory = try container.decode(CodableCategoryObject.self, forKey: .category)
    }
    
    func asPorudct() -> Product {
        return Product(
            id: id,
            name: name,
            productDescription: productDescription,
            price: price,
            savedAssetId: savedAssetId,
            category: realmCategory?.asCategory(),
            totalItem: totalItem
        )
    }
}
