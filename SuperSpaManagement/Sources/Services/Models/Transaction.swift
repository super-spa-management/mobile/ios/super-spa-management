//
//  Transaction.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 3/4/21.
//

import Foundation
import RealmSwift

struct Order {
    let transactionId: Int
    let product: Product
}

struct Transaction {
    let id: Int
    let membership: Membership
    let createDate: Date
    let totalPrice: Int
    let orderItems: [Order]
}
