//
//  ProductsService.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/23/21.
//

import Foundation
import RealmSwift
import RxSwift
import Resolver

protocol ProductsServiceType {
    func create(_ product: Product) -> Single<Void>
    func update(_ product: Product) -> Single<Void>
    
    func getProducts() -> Observable<[Product]>
}

class ProductsService: ProductsServiceType {
    
    @Injected var realmManager: RealmManagerType
    
    public static let `default` = ProductsService()
    
    func create(_ product: Product) -> Single<Void> {
        return createOrUpdate(product, update: false)
    }
    
    func update(_ product: Product) -> Single<Void> {
        return createOrUpdate(product, update: true)
    }
    
    func getProducts() -> Observable<[Product]> {
        
        return Observable
            .just(realmManager.getAll(CodableProductObject.self))
            .map { $0.toArray(type: CodableProductObject.self) }
            .map { $0.map { $0.asPorudct() } }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    private func createOrUpdate(_ product: Product, update: Bool) -> Single<Void> {
        
        return realmManager
            .create(object: product.asCodableObject(), update: update, writeBlock: { [weak self] object in
                guard let self = self else { return }
                object.id = self.realmManager.incrementID(type: CodableProductObject.self)
            })
            .observe(on: MainScheduler.instance)
    }
}
